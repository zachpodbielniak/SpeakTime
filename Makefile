
# SpeakTime - A simplistic program to speak the time correctly.
# Copyright (C) 2022 Zach Podbielniak
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


CC = gcc
ASM = nasm
STD = -std=gnu89
WARNINGS = -Wall -Wextra -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith
WARNINGS += -Wfloat-equal -Wswitch-enum -Wstrict-aliasing -Wno-missing-braces
WARNINGS += -Wno-cast-function-type -Wno-stringop-truncation #-Wno-switch-enum
DEFINES = -D _DEFAULT_SOURCE
DEFINES += -D _GNU_SOURCE
ifeq ($(BUILD_TYPE),onesdk)
DEFINES += -D BUILD_WITH_ONEAGENT_SDK
endif

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__
ifeq ($(BUILD_TYPE),onesdk)
DEFINES_D += -D BUILD_WITH_ONEAGENT_SDK
endif

OPTIMIZE = -O2 -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
#MARCH = -march=native
#MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
#CC_FLAGS += $(MARCH)
#CC_FLAGS += $(MTUNE)
CC_FLAGS += $(shell pkg-config glib-2.0 --cflags --libs)
CC_FLAGS += -lespeak-ng

CC_FLAGS_D = $(STD)
CC_FLAGS_D += $(WARNINGS)
CC_FLAGS_D += $(DEFINES_D)
#CC_FLAGS_D += -fsanitize=address
CC_FLAGS_D += $(shell pkg-config glib-2.0 --cflags --libs)
CC_FLAGS_D += -lespeak-ng

CC_FLAGS_T = $(CC_FLAGS_D)
CC_FLAGS_T += -Wno-unused-variable

ASM_FLAGS = -Wall



all:	bin speaktime
debug:	bin speaktime_d
test: 	bin $(TEST)

bin:
	mkdir -p bin/

clean: 	bin
	rm -rf bin/

check:	bin 
	cppcheck . --std=c89 -j $(shell nproc) --inline-suppr --error-exitcode=1

speaktime: 	bin
	$(CC) -o bin/speaktime Src/Main.c $(CC_FLAGS)

speaktime_d: 	bin
	$(CC) -g -o bin/speaktime Src/Main.c $(CC_FLAGS_D)






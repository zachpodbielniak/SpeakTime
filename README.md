# SpeakTime

A simplistic program to speak the time correctly. Useful with cron jobs to keep yourself aware of the time throughout the day.



## License
![AGPLv3](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)
```
SpeakTime - A simplistic program to speak the time correctly.
Copyright (C) 2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

## Building
Be sure to have the espeak-ng package on your system, as well as the libespeak-ng-dev package for development files. Run
```
make
```
Then copy ./bin/speaktime to any bin directory in your $PATH.

Debug version can be built with:
```
make debug
```

To cleanup:
```
make clean
```


## Donate
Like SpeakTime? You use it yourself? Why not donate to help make it better! I really appreciate any and all donations.

+ [PayPal](https://paypal.me/ZPodbielniak)
+ **Bitcoin** - 3C6Fc9WPH54GoVC91Sq4JTWa5C9ijKMA23
+ **Litecoin** - MVjvGjfmp3gkLniBSnreFb3SNEXq1FRxbW
+ **Ethereum** - 0xE58bAEd820308038092F732151c162f530361B59
/*

SpeakTime - A simplistic program to speak the time correctly.
Copyright (C) 2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <espeak-ng/speak_lib.h>
#include <glib-2.0/glib.h>
#include <unistd.h>



typedef struct 
{
	gchar			text_to_speak[0x100];
	gint 			hour;
	gint 			minute;
	gboolean		say_minute;
	gboolean		twenty_four;
	gboolean		run_together;
	gboolean		debug;
	gboolean		dry_run;
	gint			argc;
	gchar			**argv;
} ProgramState;




static
void 
st_print_help(
	void 
){
	g_print("speaktime\n");
	g_print("---------\n");
	g_print("A simplistic program to speak the time correctly.\n");
	g_print("\n");
	g_print("Usage:\n");
	g_print("\t-h\t| Print this message.\n");
	g_print("\t-m\t| Speak the minute, not just the hour.\n");
	g_print("\t-r\t| Say the time in 24 hour format.\n");
	g_print("\t-t\t| Run the time together. 8:00 is 'oh 8-hundred hours', rather than '8 oclock'.\n");
	g_print("\t-d\t| Print to stdout the string being spoken.\n");
	g_print("\t-D\t| Dry run, don't actually speak.\n");
	exit(0);
}




static 
void 
st_parse_args(
	ProgramState		*state
){
	gint opt;
	while (-1 != (opt = getopt(state->argc, state->argv, ":hmtrdD")))
	{
		switch(opt)
		{
			case 'm': state->say_minute = TRUE; break;
			case 't': state->twenty_four = TRUE; break;
			case 'r': state->run_together = TRUE; break;
			case 'd': state->debug = TRUE; break;
			case 'D': state->dry_run = TRUE; break;
			case 'h': st_print_help(); 
		}
	}
}




static 
void 
st_get_time(
	ProgramState		*state
){
	g_autoptr(GDateTime) now;
	now = g_date_time_new_now_local();

	state->hour = g_date_time_get_hour(now);
	state->minute = g_date_time_get_minute(now);
}




static 
void 
st_format_text(
	ProgramState		*state
){
	gint hour_to_say;
	gchar *meridiem;

	if (!state->twenty_four)
	{ hour_to_say = (state->hour > 12) ? state->hour - 12 : state->hour ;}
	else 
	{ hour_to_say = state->hour; }

	hour_to_say = (0 == hour_to_say && !(state->run_together)) ? 12 : hour_to_say;

	meridiem = (12 > state->hour) ? "A.M." : "P.M.";
	meridiem = (state->twenty_four) ? "." : meridiem;
	meridiem = (state->run_together) ? "hours" : meridiem;


	if (state->run_together)
	{
		gchar *format;

		if (state->say_minute)
		{
			format = (hour_to_say < 12) ? 
				"It is now oh %d hundred %d %s" :
				"It is now %d hundred %d %s";
		
			g_snprintf(
				state->text_to_speak,
				sizeof(state->text_to_speak) - 1,
				format,
				hour_to_say,
				state->minute,
				meridiem
			);
		}
		else 
		{
			format = (hour_to_say < 12) ? 
				"It is now oh %d hundred %s" :
				"It is now %d hundred %s";
			
			g_snprintf(
				state->text_to_speak,
				sizeof(state->text_to_speak) - 1,
				format,
				hour_to_say,
				meridiem
			);
		}

	}
	else if (state->say_minute)
	{
		g_snprintf(
			state->text_to_speak,
			sizeof(state->text_to_speak) - 1,
			"It is now %d:%02d %s",
			hour_to_say,
			state->minute,
			meridiem
		);
	}
	else 
	{
		g_snprintf(
			state->text_to_speak,
			sizeof(state->text_to_speak) - 1,
			"It is now %d oh clock %s",
			hour_to_say,
			meridiem
		);
	}

	if (state->debug)
	{ g_print("%s\n", state->text_to_speak); }
}



static 
void 
st_speak(
	ProgramState		*state
){
	if (state->dry_run)
	{ return; }

	espeak_Initialize(
		0,
		0,
		NULL,
		0
	);

	espeak_SetVoiceByName("English");
	espeak_Synth(
		state->text_to_speak,
		strlen(state->text_to_speak),
		0,
		POS_CHARACTER,
		0,
		espeakCHARS_AUTO,
		NULL,
		NULL
	);

	espeak_Synchronize();
}




gint 
main(
	gint			argc,
	gchar			**argv
){
	ProgramState state;

	memset(&state, 0x00, sizeof(ProgramState));
	state.argc = argc;
	state.argv = argv;

	st_parse_args(&state);
	st_get_time(&state);
	st_format_text(&state);
	st_speak(&state);

	return 0;
}
